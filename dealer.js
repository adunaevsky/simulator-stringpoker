var dealer =  {
    stringCards:['', '', ''],
    primaryCards:['', '', '', '', ''],
    deck: [],
    getCard(cardNum, testCase, cardTypes) {
        //  console.log( this.stringCards);

        if (typeof testCase === 'object' && cardTypes === 'stringCards' && testCase.length > 0) {

            this.stringCards[cardNum] = testCase[cardNum];

            var cardInDeck = this.deck.indexOf(this.stringCards[cardNum]);
            this.deck.splice(cardInDeck, 1);

            //  console.log(cardInDeck, this.deck.length, this.stringCards, testCase, cardNum - 3, cardNum);

        } else if (cardTypes === 'stringCards') {
            var cardsInDeck = this.deck.length;
            this.stringCards[cardNum] = this.deck[cardsInDeck - 1];
            this.deck.pop();
            //  console.log(this.stringCards);
        } else if (typeof testCase === 'object' && cardTypes === 'primaryCards' && testCase.length > 0) {
            this.primaryCards[cardNum] = testCase[cardNum];

            var cardInDeck = this.deck.indexOf(this.primaryCards[cardNum]);
            this.deck.splice(cardInDeck, 1);
        }
        else if (cardTypes === 'primaryCards') {
            var cardsInDeck = this.deck.length;
            this.primaryCards[cardNum] = this.deck[cardsInDeck - 1];
            this.deck.pop();
            //   console.log('primaryCards',this.primaryCards);
        }
    },
    swapCard(cardNum) {
      //  console.log(cardNum);
        this.primaryCards[cardNum] = this.deck[this.deck.length - 1];
        this.deck.pop();
    },
    upgradeStringCard(removeCard, newCard) {
        var cardRemoved = this.stringCards[removeCard];
       // console.log('BEFORE string swap',this.stringCards);
        this.stringCards.splice(removeCard, 1, newCard)
      //  console.log('AFTER string swap',this.stringCards);
        return cardRemoved;
    
    },
    newDeck() {
        var suits = ['C', 'H', 'S', 'D'];
        this.deck = [];
        for (var s = 0; s < suits.length; s++) {
            for (var r = 2; r <= 14; r++) {
                this.deck.push(suits[s] + r
                );
            }
        }
        this.shuffle(this.deck);
    },
    shuffle(array) {
        var i = 0, j = 0, temp = null
        for (i = array.length - 1; i > 0; i -= 1) {
            j = Math.floor(Math.random() * (i + 1))
            temp = array[i]
            array[i] = array[j]
            array[j] = temp
        }
    }
};

module.exports = dealer;