var tableValues =[{
    text: 'Royal Flush',
    clr: '#E2172D',
    reward: 800
}, {
    text: 'Straight Flush',
    bonusReward: [7],
    clr: '#E7681B',
    reward: 50
}, {
    text: '4 Aces w/ 2-4',
    clr: '#ea8623',
    reward: 400
}, {
    text: '4 2-4 w/ A-4',
    clr: '#E7A51B',
    reward: 160
}, {
    text: 'Four Aces',
    clr: '#E6C817',
    reward: 160
}, {
    text: 'Four 2-4',
    clr: '#E4EB19',
    reward: 80
}, {
    text: 'Four 5-K',
    clr: '#68EB19',
    reward: 50
}, {
    text: 'Full House',
    clr: '#24A400',
    reward: 9
}, {
    text: 'Flush',
    bonusReward: [2],
    clr: '#33A38A',
    reward: 5
}, {
    text: 'Straight',
    bonusReward: [3],
    clr: '#556FF9',
    reward: 4
}, {
    text: 'Three of a Kind',
    bonusReward: [5],
    clr: '#903AFC',
    reward: 3
}, {
    text: 'Two Pair',
    clr: '#A501A5',
    reward: 1
}, {
    text: 'Jacks or Better',
    dName: 'Pair',
    bonusReward: [1.5],
    clr: '#D701AF',
    reward: 1
}];


module.exports = tableValues;
