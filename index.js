
function setStats() {
    return {
        totalWin: 0,
        percentOfTotalWins: 0,
        regularWins: 0,
        regularWins_percentOfTotal: 0,
        bonusWins: 0,
        bonusWins_percentOfTotal: 0,
        totalCount: 0,
        percentOfTotalRounds: 0,
        rounds: {}
    }
}

const fs = require('fs');

var payTable = require('./payTable'),
    autoHolder = require('./autoHolder'),
    stringDeck = require('./dealer'),
    autoPicker = require('./autoPicker'),
    primaryDeck = require('./dealer'),
    primaryTests = require('./primaryTests'),
    bonusResults = require('./bonusResults'),
    primaryResults = require('./primaryResultsDDB'),
    primaryTable = payTable.map((r) => {
        var result = {
            hand: r.text,
            reward: r.reward
        }
        result.stats = setStats();
        return result
    }),
    bonusTable = [
        {
            bonusReward: 12,
            hand: 'Dealt String Royal',
            stats: setStats()
        }, {
            bonusReward: 9,
            hand: 'String Royal',
            stats: setStats()
        }, {
            bonusReward: 0,
            hand: 'No Bonus Win',
            stats: setStats()
        }],
    progress = {
        percentDone: [10, 20, 30, 40, 50, 60, 70, 80, 90],
        roundPercents: [],
        startTime: new Date().getTime(),
    },
    params = {
        timesPlayed: 1000000000,


        // testString: [],
        //  testPrimary: primaryTests[0].cards,
        cash: {
            coinValue: 1,
            bonusCost: 2.5,
            playCost: 1
        }
    }, virtualPayTable = {},
    overallResults = {
        bet: 0,
        totalWin: 0,
        regularWin: 0,
        bonusWin: 0,
        percentRegularWins: 0,
        percentBonusWins: 0,
        averageTotalWinPerGame: 0,
        averageBonsWinPerGame: 0,
        averageRegularWinPerGame: 0,
        bonusRounds: 0,
        regularRounds: params.timesPlayed,
        percentBonusRounds: undefined,
        roundsPerGame: { 0: 0 },
        RTP: undefined
    };

params.cash.totalGameCost = (params.cash.playCost + params.cash.bonusCost) * params.cash.coinValue;

payTable.forEach((t, i) => {
    if (t.bonusReward) {
        bonusTable.push({
            bonusReward: t.bonusReward[0],
            hand: t.text,
            stats: setStats()
        });
        if (t.dName) {
            bonusTable[bonusTable.length - 1].hand = t.dName;
        }
    }
});

bonusTable.sort(function (a, b) { return b.bonusReward - a.bonusReward });


primaryTable.push(
    {
        hand: 'Non Qualifying Pair',
        reward: 0,
        stats: setStats()
    }
);
primaryTable.push(
    {
        hand: 'High Card',
        reward: 0,
        stats: setStats()
    }
);

var pTableLabels = primaryTable.map((p) => { return p.hand });
var bTableLabels = bonusTable.map((b) => { return b.hand });

//console.log(bTableLabels, pTableLabels);

progress.percentDone.forEach((n) => {
    progress.roundPercents.push(Math.floor((n / 100) * params.timesPlayed));
});



function updateVirtualPayTable(mulitply, reward, primaryLabel, bonusLabel) {

    /*  if(reward == 400){
         
         console.log(reward, mulitply);
     } */

    var virtualReward = mulitply * reward;

    /*  if (mulitply === 10 && virtualReward > 1000) {
         console.log(virtualReward, reward, mulitply);
     } */
    /*    if(virtualReward > 1000){
           console.log(virtualReward, reward, mulitply);
       } */

    if (typeof virtualPayTable[virtualReward] === 'undefined') {
        virtualPayTable[virtualReward] = { 
            count: 1, 
            probability: undefined,
            baseReward: [reward], 
            primaryLabel: [primaryLabel], 
            bonusLabel: [bonusLabel], 
            mupltiplyer: [mulitply] };
    } else {
        virtualPayTable[virtualReward].count++;
        if (virtualPayTable[virtualReward].baseReward.indexOf(reward) === -1) {
            virtualPayTable[virtualReward].baseReward.push(reward);
            virtualPayTable[virtualReward].primaryLabel.push(primaryLabel);
        }
        if (virtualPayTable[virtualReward].mupltiplyer.indexOf(mulitply) === -1) {
            virtualPayTable[virtualReward].mupltiplyer.push(mulitply);
            virtualPayTable[virtualReward].bonusLabel.push(bonusLabel);
        }
    }

}

function playStringBonus(currentBonus, bonusRound) {

    //upgrade string bonus
    var bonusUpgrade = autoPicker.bestStringHand(primaryDeck.primaryCards, stringDeck.stringCards, currentBonus.bonus);
    if (bonusUpgrade.bonus) {
        stringDeck.upgradeStringCard(bonusUpgrade.removeStringCardNum, bonusUpgrade.pCardSwap);
    }

    //play primary one ore time (bonus round...)
    var pResult = playPrimaryCards();
    if (bonusUpgrade.bonus) {
        recordResults(pResult, bonusUpgrade, bonusRound, false);
    } else {
        recordResults(pResult, currentBonus, bonusRound, false);
    }

    if (pResult.payout > 0) {
        //repeat playing bonus round until there is no win
        if (bonusUpgrade.bonus) {
            //bonus upgraded
            playStringBonus(bonusUpgrade, bonusRound + 1);
        } else {
            //bonus kept
            playStringBonus(currentBonus, bonusRound + 1);
        }

    } else {
        //last bonus round played
        numBonsuRounds = bonusRound;
    }
}

function playPrimaryCards() {
    primaryDeck.newDeck();
    for (var primaryCard = 0; primaryCard < 5; primaryCard++) {
        // primaryDeck.getCard(primaryCard, params.testPrimary, "primaryCards");
        primaryDeck.getCard(primaryCard, [], "primaryCards");
    }

    var pHeld = autoHolder.setHolds(primaryDeck.primaryCards).result;
    for (var h = 0; h < pHeld.length; h++) {

        if (!pHeld[h]) {
            primaryDeck.swapCard(h);
        }

    }

    return primaryResults.fiveCards(primaryDeck.primaryCards);
}


function recordResults(p, b, round, initialRound) {

    var bIndex = bTableLabels.indexOf(b.label);
    //console.log(b.label, bIndex);
    //   console.log('bonus table result:',bonusTable[bIndex].hand,' vs. ', b.label)
    /*   if (b.label === 'Delta String Royal') {
          console.log('OMG....', initialRound, round);
      } */


    bonusTable[bIndex].stats.totalCount++;
    if (bonusTable[bIndex].stats.rounds[round]) {
        bonusTable[bIndex].stats.rounds[round].count++;
    } else {
        bonusTable[bIndex].stats.rounds[round] = { count: 1, win: 0 };
    }


    var pIndex = pTableLabels.indexOf(p.label);

    primaryTable[pIndex].stats.totalCount++;
    if (primaryTable[pIndex].stats.rounds[round]) {
        primaryTable[pIndex].stats.rounds[round].count++;
    } else {
        primaryTable[pIndex].stats.rounds[round] = { count: 1, win: 0 };
    }

    //console.log(b.bonus, p.payout * params.cash.playCost);

    updateVirtualPayTable(b.bonus, p.payout * params.cash.playCost, p.label, b.label);

    var won = p.payout * b.bonus * params.cash.coinValue * params.cash.playCost;

    primaryTable[pIndex].stats.totalWin += won;
    bonusTable[bIndex].stats.totalWin += won;
    overallResults.totalWin += won;

    if (initialRound) {
        overallResults.regularWin += won;
        primaryTable[pIndex].stats.regularWins += won;
        bonusTable[bIndex].stats.regularWins += won;
    } else {
        overallResults.bonusWin += won;
        primaryTable[pIndex].stats.bonusWins += won;
        bonusTable[bIndex].stats.bonusWins += won;
    }

    primaryTable[pIndex].stats.rounds[round].win += won;
    bonusTable[bIndex].stats.rounds[round].win += won;

    /*        console.log('value won:',won);
           console.log('bonus results');
           console.log(bonusTable[bIndex].stats, bonusTable[bIndex].hand);
           console.log('primary results');
           console.log(primaryTable[pIndex].stats, primaryTable[pIndex].hand); */



    //  console.log(bonusTable[bIndex].hand, bonusTable[bIndex].stats.rounds, bonusTable[bIndex].stats.totalCount)
    //   console.log(primaryTable[pIndex].hand, primaryTable[pIndex].stats.rounds,'total count:' ,primaryTable[pIndex].stats.totalCount,'round num: ', round)
    //bonusTable
}

function recordOverall(numBonusRounds) {


    overallResults.bonusRounds += numBonusRounds;



    if (numBonusRounds === 0) {
        //console.log('no bonus rounds!')
        overallResults.roundsPerGame[0]++;


    } else {
        if (overallResults.roundsPerGame[numBonusRounds]) {
            overallResults.roundsPerGame[numBonusRounds]++;
        } else {
            overallResults.roundsPerGame[numBonusRounds] = 1;
        }
    }

    //console.log(overallResults);

}

console.time('simulationTime');

var numBonsuRounds = 0;
var costPerGame = (params.cash.bonusCost + params.cash.playCost) * params.cash.coinValue;

for (var r = 0; r < params.timesPlayed; r++) {
    numBonsuRounds = 0;
    stringDeck.newDeck();

    overallResults.bet += costPerGame;

    var checkIndex = progress.roundPercents.indexOf(r);
    if (checkIndex > -1) {
        /* ENABLE THIS FOR LONG RUNS! */
        console.log(progress.percentDone[checkIndex] + '% done in', new Date().getTime() - progress.startTime + ' ms (round ' + r + ') RTP:', overallResults.totalWin / overallResults.bet * 100);
    }

    //get 3 cards for string hand
    for (var stringCard = 0; stringCard < 3; stringCard++) {
        // stringDeck.getCard(stringCard, params.testString, "stringCards");
        stringDeck.getCard(stringCard, [], "stringCards");
    }
    var bResult = bonusResults.threeCards(stringDeck.stringCards, 0, false);
    var pResult = playPrimaryCards();

    recordResults(pResult, bResult, 0, true);

    if (pResult.payout > 0) {
        playStringBonus(bResult, 1, bResult);
    }

    recordOverall(numBonsuRounds);
    // console.log('-----')
    // console.log(r);
}


function reportResults() {

    overallResults.RTP = overallResults.totalWin / overallResults.bet * 100;

    bonusTable.forEach((b) => {
        b.stats.percentOfTotalWins = b.stats.totalWin / overallResults.totalWin * 100;
        b.stats.percentOfTotalRounds = b.stats.totalCount / (overallResults.bonusRounds + overallResults.regularRounds) * 100;
        b.stats.regularWins_percentOfTotal = b.stats.regularWins / overallResults.totalWin * 100
        b.stats.bonusWins_percentOfTotal = b.stats.bonusWins / overallResults.totalWin * 100
    });
    primaryTable.forEach((b) => {
        b.stats.percentOfTotalWins = b.stats.totalWin / overallResults.totalWin * 100;
        b.stats.percentOfTotalRounds = b.stats.totalCount / (overallResults.bonusRounds + overallResults.regularRounds) * 100;
    });

    bonusTable.forEach((b) => {
        var result = { hand: b.hand }
        if (typeof b.stats.rounds !== 'undefined') {
            // console.log(b.stats.rounds);
            result.roundKeys = [];
            result.roundCounts = [];
            result.roundWins = [];
            for (var key in b.stats.rounds) {
                result.roundCounts.push(b.stats.rounds[key].count);
                result.roundWins.push(b.stats.rounds[key].win);
                result.roundKeys.push(key)
            }
        }
        return result;
    });

    primaryTable.forEach((b) => {
        var result = { hand: b.hand }
        if (typeof b.stats.rounds !== 'undefined') {
            // console.log(b.stats.rounds);
            result.roundKeys = [];
            result.roundCounts = [];
            result.roundWins = [];
            for (var key in b.stats.rounds) {
                result.roundCounts.push(b.stats.rounds[key].count);
                result.roundWins.push(b.stats.rounds[key].win);
                result.roundKeys.push(key);
            }
        }
        return result;
    });

    overallResults.percentBonusRounds = overallResults.bonusRounds / overallResults.regularRounds * 100;
    overallResults.percentRegularWins = overallResults.regularWin / overallResults.totalWin * 100;
    overallResults.percentBonusWins = overallResults.bonusWin / overallResults.totalWin * 100;

    overallResults.averageTotalWinPerGame = overallResults.totalWin / overallResults.regularRounds;
    overallResults.averageBonsWinPerGame = overallResults.bonusWin / overallResults.regularRounds;
    overallResults.averageRegularWinPerGame = overallResults.regularWin / overallResults.regularRounds;

    for (var key in virtualPayTable) {
        virtualPayTable[key].probability = virtualPayTable[key].count / (overallResults.bonusRounds + overallResults.regularRounds);
    }

    /* 
        console.log('---- Bonus Table ----')
        console.log(bonusTable);
        console.log('----- Bonus Table counds ----')
        console.log(bonusRoundCounts);
        console.log('----- Primary Table ----')
        console.log(primaryTable);
        console.log('----- Primary Table Counts ----')
        console.log(primaryRoundCounts);
        console.log('----- PARAMS: ----')
        console.log(params); */
  /*   console.log('----- Overall: ----')
    console.log(overallResults); */
    console.log(virtualPayTable);
    console.log(overallResults.RTP); 

    let data = JSON.stringify({
        parameters: params,
        bonusTable: bonusTable,
        primaryTable: primaryTable,
        virtualPayTable: virtualPayTable,
        overallResults: overallResults
    });
    fs.writeFileSync('Raw Results/' + progress.startTime + '.json', data);


}

reportResults();


console.timeEnd('simulationTime');